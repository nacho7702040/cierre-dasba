import pandas as pd
import numpy as np
import folium
import matplotlib.pyplot as plt
from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter
import time

#Evaluación de modelos
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

#Función para mostrar un dataframe geolocalizado en un mapa

def dibujar_mapa (df, lat, lon, valor_mostrar):
    # Creamos un mapa centrado en las coordenadas iniciales
    map_center = [df[lat].mean(), df[lon].mean()]
    my_map = folium.Map(location=map_center, zoom_start=7)

    # Añadimos marcadores al mapa
    for index, row in df.iterrows():
        folium.Marker([row[lat], row[lon]], popup=row[valor_mostrar]).add_to(my_map)
    
    return my_map

#Función para obtener el municipio pasando una coordenadas

def obtener_municipio(latitud, longitud):
    # Inicializar el geocodificador (Nominatim)
    geolocator = Nominatim(user_agent="my_geocoder")

    # Añadimos un limitador de descargas
    geocode = RateLimiter(geolocator.reverse, min_delay_seconds=0.4)
    
    # Realizar una búsqueda inversa para obtener la información de la ubicación
    location = geocode((latitud, longitud), language='es')
       
    # Extraer el nombre del municipio
    address = location.raw.get('address', {})  
    municipio = address.get('city', None) or address.get('town', None) or address.get('municipaly', None)
    
    return municipio

#Función para obtener el barrio pasando una coordenadas

def obtener_barrio(latitud, longitud):
    # Inicializar el geocodificador (Nominatim)
    geolocator = Nominatim(user_agent="my_geocoder")

    # Añadimos un limitador de descargas
    geocode = RateLimiter(geolocator.reverse, min_delay_seconds=0.4)
    
    # Realizar una búsqueda inversa para obtener la información de la ubicación
    location = geocode((latitud, longitud), language='es')
       
    # Extraer el nombre del municipio
    address = location.raw.get('address', {})
    
    municipio = address.get('city_district', None) or address.get('suburb', None)
    
    return municipio

#Función para evaluar y graficar los resultados de los modelos

def evaluar_modelo (modelo, X_test, y_test):
    # Realizamos las predicciones del modelo
    y_pred = modelo.predict(X_test)

    # Evaluamos el modelo
    mse = mean_squared_error(y_test, y_pred)
    r2 = r2_score(y_test, y_pred)
    mae = mean_absolute_error(y_test, y_pred)
    rmse = mean_squared_error(y_true=y_test, y_pred=y_pred, squared=False)

    print(f'Error cuadrático medio (MSE): {mse}')
    print(f'Raíz del Error Cuadrático Medio (RMSE): {rmse}')
    print(f'R-cuadrado (R²): {r2}')
    print(f'MAE en el conjunto de prueba: {mae}')

    #Graficamos el resultado del modelo frente a la predicción

    plt.scatter(y_test, y_pred)
    plt.xlabel('Precios reales')
    plt.ylabel('Predicciones')
    plt.title('Predicciones vs. Precios reales')
    plt.show()

    return


 