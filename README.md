## Descripción

El objetivo del proyecto es realizar un análisis de precios de bienes raíces de Madrid y Barcelona, realizar un modelo de predicción de precios y detectar posibles oportunidades de inversión.

## Autor

Ignacio Calvo Artola - https://www.linkedin.com/in/nachocalvoartola/
